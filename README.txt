Commerce Stock Module 7.x-2.0
==============================
This module provides a real time display of stock for Drupal Commerce stores.

This module includes :
- A rules actions to sends a decrease stock event through NodeJS server.
- A rules to decrease stock in real time.


To install and get working
============================
1. Download commerce_live_stock.
2. Enable the Commerce Live Stock.

Dependencies
============
- Node JS
- Commerce Stock API
- Commerce Simple Stock

Important:
  You may start your Node JS server after enable Commer Live Stock module.


rules configuration
===================
If you need to make changes to rules you also need the permission
"Make rule based changes to commerce stock".
